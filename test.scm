(import (scheme base)
        (scheme write)
        (scheme file)
        (retropikzel string-util v1-0-1 main)
        (srfi 64))

(test-begin "contains-char?")
(test-assert (string-util-contains-char? "lol.lel;lul" #\.))
(test-end "contains-char?")

(test-begin "split-by-string")
(define hand-splitted '("lol" "lel" "lul"))
(define test-string-1 "lol===lel===lul")
(define auto-splitted (string-util-split-by-string test-string-1 "==="))
(test-equal hand-splitted auto-splitted)
(test-end "split-by-string")

(test-begin "string-util-count-char")
(test-equal 6 (string-util-count-char "   bar foo  " #\space))
(test-end "string-util-count-char")

(test-begin "string-util-contains-char?")
(test-assert (string-util-contains-char? "   bar foo  " #\space))
(test-end "string-util-contains-char?")

(test-begin "string-util-strip-prefix")
(test-equal "bar foo  " (string-util-strip-prefix "   bar foo  " " "))
(test-end "string-util-strip-prefix")

(test-begin "string-util-strip-suffix")
(test-equal "   bar foo" (string-util-strip-suffix"   bar foo  " " "))
(test-end "string-util-strip-suffix")

(test-begin "string-util-in-list?")
(test-equal #t (string-util-in-list? "bar" (list "foo" "bar")))
(test-end "string-util-in-list?")

(test-begin "string-util-starts-with?")
(test-equal #f (string-util-starts-with? "foobar" "bar"))
(test-equal #t (string-util-starts-with? "foobar" "foo"))
(test-end "string-util-starts-with?")

(test-begin "string-util-starts-with-any?")
(test-equal #f (string-util-starts-with-any? "foobar" (list "bar" "one")))
(test-equal #t (string-util-starts-with-any? "foobar" (list "bar" "one" "foo")))
(test-end "string-util-starts-with-any?")

(test-begin "string-util-ends-with?")
(test-equal #t (string-util-ends-with? "foobar" "bar"))
(test-equal #f (string-util-ends-with? "foobar" "foo"))
(test-end "string-util-ends-with?")

(test-begin "string-util-join")
(test-equal "foo-bar-fii-bur" (string-util-join (list "foo" "bar" "fii" "bur")
                                                "-"))
(test-end "string-util-join")

(test-begin "string-util-replace-all")
(test-equal #t (string=? "foo-bar-fii-bur" (string-util-replace-all "foo*bar*fii*bur" "*" "-")))
(test-equal #t (string=? "foo bar-fii bur" (string-util-replace-all "foo bar*fii bur" "*" "-")))
(test-equal #t (string=? "foo bar-fii-bur" (string-util-replace-all "foo bar**fii**bur" "**" "-")))
(test-end "string-util-replace-all")

(test-begin "string-util-replace-count")
(test-equal "foo-bar*fii*bur" (string-util-replace-count "foo*bar*fii*bur" "*" "-" 1))
(test-equal "foo bar-fii bur" (string-util-replace-count "foo bar*fii bur" "*" "-" 1))
(test-equal "foo bar-fii bur" (string-util-replace-count "foo bar*fii bur" "*" "-" 2))
(test-end "string-util-replace-count")

(test-begin "string-util-replace-with-tags")
(test-equal "foo bar<em>fii</em>bur" (string-util-replace-with-tags "foo bar*fii*bur" "*" "<em>" "</em>"))
(test-equal "foo bar<strong>fii</strong>bur" (string-util-replace-with-tags "foo bar**fii**bur" "**" "<strong>" "</strong>"))
(test-end "string-util-replace-with-tags")
