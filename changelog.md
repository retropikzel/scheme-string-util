# Changelog

## 2024-02-16

- Added string-util-ends-with?
- Added schubert generated documentation
- Added
  - string-util-replace-all
  - string-util-replace-count
  - string-util-replace-with-tags
