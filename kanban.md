# Ideas

## string-util-replace-character

Takes as arguments a string and two characters, replaces the first character
with the second

## string-util-replace-characters

Takes as arguments a string and alist of character pairs, replaces the car
characters in the string with the cdr characters.

## string-util-split-by-strings

In addition to string, takes as arguments a list of strings and splits on
any of them

## string-util-split-by-characters

In addition to string, takes as arguments a list of characters and splits on
any of them

## string-util-split

Internally use string-util-split-by-string, string-util-split-by-character,
string-util-split-by-strings, string-util-split-by-characters
but determine which one to use from the second arguments type

## string-util-column
https://dlang.org/phobos/std_string.html#.column
## string-util-shorten

(string-util-shorten "Hello world" 5 "...")
> "Hello..."
## string-util-translate
(string-util-translate "Hello" (list (cons #\H #\O) (cons #\o #\h))
> "Oellh"
## string-util-wrap
(string-util-wrap "Hello" 3)
> (list "Hel" "lo")
## string-util-tab-expand
(string-util-tab-expand "\tHello" 4)

> "    Hello"
## string-util-uppercase
## string-util-lowercase
## string-util-capitalize
## string-util-right-justify
(string-util-right-justify "Hello" 7 #\X)
> "XXHello"
## (string-util-left-justify "Hello" 7 #\X)
> "HelloXX"
## string-util-center
(string-util-center "Hello" 7 #\X)
> "XHelloX"
## string-util-last-index-of
Returns the last index of given character in string.
## string-util-last-index-of-any
Returns the last index of any of the given characters.
## Index functions return #f if no index is found
## string-util-numeric?
Returns true if string can be converted to number.
## string-util-index-of-any
Returns the first index of any given characters.

(string-util-index-of-any "Hello world" (list #\o #\w))
> 5
## string-util-index-of
Get the index of first occurence of character in string
## string-util-indexes-of
Get list of indexes of character in string.
## string-util-interpolate
Macro(?) that can be used to interpolate strings.

(define var1 "Hello") (define var2 "World) (string-util-interpolate "this is $var1 and $var2 example")
> "this is Hello and World example"

Making this a macro will propably make it fastest.
If we imagine that our example would be turned into
(string-append "this is" var1 " and " var2 " example").

It also needs to add conversion to string, so if var1 is number then it should
be (string-append "this is" (number->string var1) " and " var2 " example").
## string-util-surround
Takes as argument a string to surround, and another string to surround with.

(string-util-surround "SchemeSlayer" "xxx")
> "xxxSchemeSlayerxxx"
## string-util-join
Takes as argument a list of strings to join and another string to insert between the strings.

(string-util-join ("lol" "lel" "lul") "-")
> "lol-lel-lul"



















